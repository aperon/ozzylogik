/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SRVCON_HPP
#define _SRVCON_HPP

#include <thread>

#include <stdint.h>

class Server;

class Connection
{
public:
    ~Connection();
    Connection (int socket, int timeout);

    bool isRunning() const;
    void stopRunning();
    void start();

private:
    void dataExchange();
    bool writeResult (bool isError);
    bool write (char *buffer, size_t size);
    bool read (char *buffer, size_t size);

private:
    static void handleThread (Connection *connection);
    static void htond (double &x);
    static void ntohd (double &x);

private:
    Connection()                                     = delete;
    Connection(const Connection &other)              = delete;
    Connection &operator = (const Connection &other) = delete;

private:
    int      _socket;

    uint32_t _readBytes    = 0;
    uint32_t _writeBytes   = 0;
    uint8_t  _compressRatio= 0;

    bool     _running = true;
    double   _increment;
    uint32_t _size = 1000000;
    std::unique_ptr< std::thread >_thread;

private:
    static constexpr size_t   _closeSocket = 500000;  // time to close the socket
    static constexpr uint32_t _magic       = 0x53545259;
    static constexpr uint32_t _version     = 17;
};

inline bool Connection::isRunning() const
{
    return _running;
}

inline void Connection::stopRunning()
{
    _running = false;
}

#endif //_SRVCON_HPP
