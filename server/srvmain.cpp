/*
Copyright (C) 2021 by Alex Perchatochnikov <aperon.all@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config.hpp"
#include "srvmain.hpp"
#include "srvsrv.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <iostream>

#include <signal.h>
#include <stdint.h>

static constexpr uint16_t default_port = 8765;

static Server *server = nullptr;

static void sig_handler (int signum)
{
    if (signum == SIGINT || signum == SIGTERM)
    {
        std::cout << "\nReceived signal Ctrl-C" << std::endl;
        BOOST_LOG_TRIVIAL(info) << "Received signal Ctrl-C";

        if (server)
            server->stop();
    }
}

int main ([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
{
    uint16_t port = default_port;

    std::cout << "ozzylogik server"
              << OZZYLOGIK_VERSION_STRING
              << "  Copyright (C) 2021 aperon.all@gmail.com" << std::endl;

    // init log
    logging::add_file_log ( keywords::file_name = "ozzylogik_%N.log",       // file name pattern
                            keywords::rotation_size = 10 * 1024 * 1024,     // rotate files every 10 MiB or at midnight
                            keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                            keywords::auto_flush = true,
                            keywords::format = "[%TimeStamp%]: %Message%"); // log record format

    logging::core::get()->set_filter( logging::trivial::severity >= logging::trivial::info);
    logging::add_common_attributes();

    // start logging
    BOOST_LOG_TRIVIAL(info) << "Start server";

    // get config
    try
    {
        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini("/etc/ozzylogik.conf", pt);
        std::string str_port = pt.get< std::string >("Server.port");

        if (!str_port.empty())
        {
            uint16_t value;

            try
            {
                value = boost::lexical_cast< uint16_t >(str_port);
                port = value;
            }
            catch (boost::bad_lexical_cast const &e)
            {   // bad input
                BOOST_LOG_TRIVIAL(error) << "Bad config file";
            }
        }
        else
        {
            port = default_port;
        }
    }
    catch (...)
    {
        port = default_port;
    }

    BOOST_LOG_TRIVIAL(info) << "Config file is ready";

    // FIXME: if there is an exception at this place, then it's pointless to restore something
    // How can I pass this to sig_handler function?
    // std::unique_ptr< Server > server = std::make_unique< Server >(port);
    server = new Server( port );
    if (!server->init())
    {
        BOOST_LOG_TRIVIAL(error) << "Fatal error into server";
        return 1;
    }

    // Ctrl-C catch
    signal( SIGINT,  sig_handler);
    signal( SIGTERM, sig_handler);

    signal( SIGPIPE, sig_handler);

    server->start();
    delete server;

    BOOST_LOG_TRIVIAL(info) << "Stop server";
}
