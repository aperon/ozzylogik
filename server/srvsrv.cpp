/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "srvsrv.hpp"
#include "srvmain.hpp"

#include <thread>
#include <iostream>

#include <poll.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>

Server::~Server()
{
    if (_socket > 0)
    {
        BOOST_LOG_TRIVIAL(info) << "Closing server socket " <<_socket << " with port " <<_port;

        // for BSD-like systems
        ::shutdown(_socket, SHUT_RDWR);

        ::close(_socket );
        ::usleep(_closeSocket );
    }
}

Server::Server (uint16_t port)
    :_port (port)
{
}

bool Server::init()
{
    if (!makeSocket())
        return false;

    return true;
}

bool Server::start()
{
    int ret;
    int nfds = 1;

    // more than twenty connections at the same time is unrealistic
    struct pollfd fds[20];

    // set it up to accept connections
    if (::listen(_socket, 1) < 0)
    {
        BOOST_LOG_TRIVIAL(error) << ::strerror(errno);
        return false;
    }

    // initialize the pollfd structure
    memset( fds, 0 , sizeof(fds));
    fds[0].fd     =_socket;
    fds[0].events = POLLIN;

    BOOST_LOG_TRIVIAL(info) << "Start listening on " <<_port << " port. Press Ctrl-C to exit..";

    while (_running)
    {
        // hemorrhoid treatment with blocking the "accept" function
        ret = ::poll( fds, nfds,_srvTimeout);
        if (ret == -1)
        {
            if (errno != EINTR)
                BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;

            _running = false;
            continue;
        }
        else if (!ret) // timeout
        {
            // garbage collector, clear empty threads
            _mutexThreads.lock();
            for (auto it =_listThreads.begin(); it !=_listThreads.end();)
            {
                if ((*it)->isRunning() == false)
                {
                    delete *it;
                    it = _listThreads.erase( it );
                }
                else
                {
                    ++it;
                }
            }
            _mutexThreads.unlock();

            continue;
        }

        int current_size = nfds;

        // service all the sockets with input pending
        for (int i = 0; i < current_size; ++i)
        {
            if(fds[i].revents == 0)
                continue;

            if (fds[i].revents & POLLIN)
            {
                struct sockaddr_in client_addr;
                ::memset( &client_addr, 0, sizeof(client_addr));
                socklen_t client_len = sizeof(client_addr);

                int new_sd = ::accept(_socket, (struct sockaddr *)&client_addr, &client_len);
                if (new_sd < 0)
                {
                    BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;

                    _running = false;
                    continue;
                }

                // do we need a ipv6?
                char client_ipv4_str[INET_ADDRSTRLEN];
                ::inet_ntop( AF_INET, &client_addr.sin_addr, client_ipv4_str, INET_ADDRSTRLEN);

                BOOST_LOG_TRIVIAL(info) << "Incoming connection from "
                                        << client_ipv4_str
                                        << ":"
                                        << client_addr.sin_port
                                        << ". New socket is "
                                        << new_sd;
                try
                {
                    // std::unique_ptr< Connection > thr = std::make_unique< Connection >( new_sd,_srvTimeout);
                    Connection *thr = new Connection(new_sd,_srvTimeout);
                    thr->start();

                    std::lock_guard< std::mutex > lock(_mutexThreads );
                    _listThreads.insert( thr );
                }
                catch (std::system_error &e)
                {
                    BOOST_LOG_TRIVIAL(error) << e.what();

                    ::shutdown( new_sd, SHUT_RDWR);
                    ::close( new_sd );
                    ::usleep(_closeSocket );

                    _running = false;
                    continue;
                }
                catch ( std::bad_alloc &e)
                {
                    BOOST_LOG_TRIVIAL(error) << e.what();

                    ::shutdown( new_sd, SHUT_RDWR);
                    ::close( new_sd );
                    ::usleep(_closeSocket );

                    _running = false;
                    continue;
                }
            }
        }
    } // while (_running)

    BOOST_LOG_TRIVIAL(info) << "Stop listening...";

    ::usleep(_closeSocket );

    // let the threads terminate gracefully
    while (_listThreads.size())
    {
        for (auto it =_listThreads.begin(); it !=_listThreads.end();)
        {
            if ((*it)->isRunning() == false)
            {
                delete (*it);
                it = _listThreads.erase( it );
            }
            else
            {
                ++it;
            }
        }

        ::usleep(_closeSocket * 10);
    }

    return true;
}

bool Server::makeSocket()
{
    int ret;
    int on = 1;
    struct sockaddr_in name;

    // create the socket
    _socket = ::socket( AF_INET, SOCK_STREAM, 0);
    ::usleep(_openSocket );

    if (_socket < 0)
    {
        BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;
        return false;
    }

    // allow socket descriptor to be reuseable
    ret = ::setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
    if (ret < 0)
    {
        BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;
        return false;
    }

    // set socket to be nonblocking
    ret = ::ioctl(_socket, FIONBIO, (char *)&on);
    if (ret < 0)
    {
        BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;
        return false;
    }

    // give the socket a name
    ::memset( &name, 0, sizeof(name));

    name.sin_family      = AF_INET;
    name.sin_port        = htons(_port) ;
    name.sin_addr.s_addr = htonl( INADDR_ANY );

    if (::bind(_socket, (struct sockaddr *)&name, sizeof(name)) < 0)
    {
        BOOST_LOG_TRIVIAL(error) << ::strerror(errno) << " Errno: "<< errno;
        return false;
    }

    return true;
}
