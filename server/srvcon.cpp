/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "config.hpp"
#include "srvcon.hpp"
#include "srvsrv.hpp"
#include "srvmain.hpp"

#include <cctype>
#include <iostream>

#include <poll.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/sendfile.h>

/* static */ void Connection::ntohd (double &x)
{
    // WARNING: only if both platforms use the same coding schema for double and if int has the same size of long
    int *double_overlay = (int *)&x;
    int holding_buffer  = double_overlay[0];

    double_overlay [0] = ::ntohl(double_overlay [1]);
    double_overlay [1] = ::ntohl(holding_buffer);
}

/* static */ void Connection::htond (double &x)
{
    // WARNING: only if both platforms use the same coding schema for double and if int has the same size of long
    int *double_overlay = (int *)&x;
    int holding_buffer  = double_overlay[0];

    double_overlay [0] = ::htonl(double_overlay [1]);
    double_overlay [1] = ::htonl(holding_buffer);
}

/* static */ void Connection::handleThread (Connection *connection)
{
    BOOST_LOG_TRIVIAL(info) << connection->_socket << " Connection " << connection << " ready to read/write";

    connection->dataExchange();
    connection->stopRunning();
}

Connection::~Connection()
{
    stopRunning();

    BOOST_LOG_TRIVIAL(info) <<_socket << " Connection's socket closing..";

    if (_thread->joinable())
        _thread->join();

    // for BSD-like systems
    ::shutdown(_socket, SHUT_RDWR);

    ::close(_socket );
    ::usleep(_closeSocket );
}

Connection::Connection (int socket, int timeout)
    :_socket (socket)
{
    // set timeout for client
    struct timeval tv;
    tv.tv_sec  = timeout;
    tv.tv_usec = 0;

    ::setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof( struct timeval));
}

void Connection::start()
{
    _thread.reset(new std::thread(handleThread, this));
}

bool Connection::write (char *buffer, size_t size)
{
    size_t total = size;
    size_t sent  = 0;

    do
    {
        int bytes = ::write(_socket, buffer + sent, total - sent);
        if (bytes < 0)
        {
            BOOST_LOG_TRIVIAL(error) << ::strerror(errno);
            return false;
        }

        if (bytes == 0)
            break;

        sent += bytes;
    }
    while (sent < total);

    BOOST_LOG_TRIVIAL(error) << "Stop server writing. Sent = " << sent << ", Total = " << total;
    return true;
}

bool Connection::read (char *buffer, size_t size)
{
    size_t total = size;
    size_t sent  = 0;

    do
    {
        int bytes = ::recv(_socket, buffer + sent, total - sent, 0);
        if (bytes < 0)
        {
            BOOST_LOG_TRIVIAL(error) << ::strerror(errno);
            return false;
        }

        if (bytes == 0)
            break;

        sent += bytes;
    }
    while (sent < total);

    BOOST_LOG_TRIVIAL(error) << "Stop server reading. Received = " << sent << ", Total = " << total;
    return true;
}

bool Connection::writeResult (bool isError)
{
    uint32_t error = (isError == true) ? 0 : 1;
    error = ::htonl(error);

    if (!write( (char *)&error, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Unable to write result";
        return false;
    }

    return true;
}

void Connection::dataExchange()
{
    uint32_t input_magic, input_version;

    // read magic
    if (!read((char *)&input_magic, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Invalid magic length";

        writeResult(true);
        return;
    }

    // network to host
    input_magic = ::ntohl(input_magic);
    if (input_magic !=_magic)
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Wrong magic";

        writeResult(true);
        return;
    }

    if (!writeResult(false))
        return;

    // read version
    if (!read((char *)&input_version, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Invalid version length";

        writeResult(true);
        return;
    }

    // network to host
    input_version = ::ntohl(input_version);
    if (input_version !=_version)
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Wrong version";

        writeResult(true);
        return;
    }

    if (!writeResult(false))
        return;

    // read increment
    if (!read((char *)&_increment, sizeof(double)))
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Invalid increment length";

        writeResult(true);
        return;
    }

    ntohd(_increment);
    if (_increment > 1.0)
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Wrong increment";

        writeResult(true);
        return;
    }

    if (!writeResult(false))
        return;

    BOOST_LOG_TRIVIAL(info) <<_socket << " Increment = " <<_increment;

    // send size of array
    uint32_t send_size = ::htonl(_size);
    if (!write((char *)&send_size, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_socket << " Unable to send size of array";
        return;
    }

    BOOST_LOG_TRIVIAL(info) <<_socket << " Send data with size = " <<_size;

    // README: I see no point in dividing a huge array into blocks.
    // In any case, the system performs well with tens of gigabytes of dedicated memory.
    // But we must choose a strategy - optimize the program for speed or memory consumption.
    {
        std::unique_ptr< double[] > double_buffer = std::make_unique< double[] >(_size);
        size_t header =_size * sizeof(double);

        double value = 0.0;
        for (size_t i = 0; i < _size; ++i)
        {
            double_buffer[i] = value;
            htond(double_buffer[i]);
            value +=_increment;
        }

        if (!write((char *)&double_buffer[0], header))
        {
            BOOST_LOG_TRIVIAL(error) <<_socket << " Unable to write array";
            return;
        }
    }
}
