/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SRVSRV_HPP
#define _SRVSRV_HPP

#include "srvcon.hpp"

#include <set>
#include <mutex>
#include <thread>

#include <stdint.h>

class Server
{
public:
    ~Server();
    Server (uint16_t port);

    bool init();
    bool start();
    void stop();

    bool isRunning() const;

private:
    Server ()                                = delete;
    Server (const Server &other)             = delete;
    Server &operator = (const Server &other) = delete;

    bool makeSocket();

private:
    static constexpr int    _srvTimeout  = 5000;    // initialize the timeout for ::poll
    static constexpr size_t _closeSocket = 500000;  // time to close the socket
    static constexpr size_t _openSocket  = 500000;  // time to open the socket

private:
    bool     _running = true;
    int      _socket  = 0;
    uint16_t _port;

    std::mutex _mutexThreads;
    std::set< Connection* >_listThreads;
};

inline bool Server::isRunning() const
{
    return _running;
}

inline void Server::stop()
{
    _running = false;
}

#endif //_SRVSRV_HPP
