/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CLTCLT_HPP
#define _CLTCLT_HPP

#include <string>

#include <stdint.h>

class Client
{
public:
    ~Client();
    Client (uint16_t port, std::string &addr);

    bool init();
    void start();
    void stop();

private:
    bool writeResult (bool isError);
    int readResult();
    bool write (char *buffer, size_t size);
    bool read (char *buffer, size_t size);

private:
    Client ()                                = delete;
    Client (const Client &other)             = delete;
    Client &operator = (const Client &other) = delete;

private:
    static void htond (double &x);
    static void ntohd (double &x);

private:
    bool _connect = true;
    int  _socket  = 0;

    uint16_t    _port;
    std::string _addr;
    std::string _pid;

private:
    static constexpr uint32_t _magic   = 0x53545259;
    static constexpr uint32_t _version = 17;
    static constexpr double _increment = 0.5;
};


inline void Client::stop()
{
    _connect = false;
}

#endif //_CLTCLT_HPP
