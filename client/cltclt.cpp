/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2021  Alex Perchatochnikov <aperon.all@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "config.hpp"
#include "cltclt.hpp"
#include "cltmain.hpp"

#include <cctype>
#include <string>
#include <iostream>
#include <fstream>

#include <poll.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>

/* static */ void Client::ntohd (double &x)
{
    // WARNING: only if both platforms use the same coding schema for double and if int has the same size of long
    int *double_overlay = (int *)&x;
    int holding_buffer  = double_overlay[0];

    double_overlay [0] = ::ntohl(double_overlay [1]);
    double_overlay [1] = ::ntohl(holding_buffer);
}

/* static */ void Client::htond (double &x)
{
    // WARNING: only if both platforms use the same coding schema for double and if int has the same size of long
    int *double_overlay = (int *)&x;
    int holding_buffer  = double_overlay[0];

    double_overlay [0] = ::htonl(double_overlay [1]);
    double_overlay [1] = ::htonl(holding_buffer);
}

Client::~Client()
{
    if (_socket > 0)
    {
        BOOST_LOG_TRIVIAL(info) <<_pid << " Closing socket " <<_socket << " with port " <<_port;

        // for *BSD-like systems
        ::shutdown(_socket,SHUT_RDWR);

        ::close(_socket );
        ::sleep(1);
    }
}

Client::Client (uint16_t port, std::string &addr)
    :_port (port)
    ,_addr (addr)
{
    _pid = std::to_string(getpid());
}

bool Client::init()
{
    // create socket
    _socket = ::socket( AF_INET, SOCK_STREAM, 0);
    ::usleep(200000); // 200 ms

    if (_socket < 0)
    {
        BOOST_LOG_TRIVIAL(error) <<_pid << " " << ::strerror(errno);
        return false;
    }

    // set up address
    struct sockaddr_in server_addr;
    ::memset( &server_addr, 0, sizeof(server_addr));

    server_addr.sin_family      = AF_INET;
    server_addr.sin_addr.s_addr = ::inet_addr(_addr.c_str());
    server_addr.sin_port        = htons(_port );

    if (::connect(_socket, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) != 0)
    {
        BOOST_LOG_TRIVIAL(error) <<_pid << " " << ::strerror(errno);
        return false;
    }

    BOOST_LOG_TRIVIAL(error) <<_pid
                             << " Connected to "
                             <<_addr
                             << ":"
                             <<_port
                             << ". Socket is "
                             <<_socket;
    return true;
}

bool Client::write (char *buffer, size_t size)
{
    size_t total = size;
    size_t sent  = 0;

    do
    {
        int bytes = ::write(_socket, buffer + sent, total - sent);
        if (bytes < 0)
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << ::strerror(errno);
            return false;
        }

        if (bytes == 0)
            break;

        sent += bytes;
    }
    while (sent < total);

    BOOST_LOG_TRIVIAL(error) <<_pid << " Stop client writing. Sent = " << sent << ", Total = " << total;
    return true;
}

bool Client::read (char *buffer, size_t size)
{
    size_t total = size;
    size_t sent  = 0;

    do
    {
        int bytes = ::recv(_socket, buffer + sent, total - sent, 0);
        if (bytes < 0)
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << ::strerror(errno);
            return false;
        }

        if (bytes == 0)
            break;

        sent += bytes;
    }
    while (sent < total);

    BOOST_LOG_TRIVIAL(error) <<_pid << " Stop client reading. Received = " << sent << ", Total = " << total;
    return true;
}

bool Client::writeResult (bool isError)
{
    uint32_t error = (isError == true) ? 0 : 1;
    error = ::htonl(error);

    if (!write( (char *)&error, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to write result";
        return false;
    }

    return true;
}

int Client::readResult()
{
    uint32_t result;

    if (!read((char *)&result, sizeof(uint32_t)))
    {
        BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to read result";
        return -1;
    }

    return ::ntohl(result);
}

void Client::start()
{
    while (_connect)
    {
        // send magic
        uint32_t send_magic = ::htonl(_magic);
        if (!write((char *)&send_magic, sizeof(uint32_t)))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to send magic";
            return;
        }

        if ((readResult() < 1))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Wrong magic";
            return;
        }

        // send version
        uint32_t send_version = ::htonl(_version);
        if (!write((char *)&send_version, sizeof(uint32_t)))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to send version";
            return;
        }

        if ((readResult() < 1))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Wrong version";
            return;
        }

        // send increment
        double send_increment =_increment;
        htond(send_increment);
        if (!write((char *)&send_increment, sizeof(double)))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to send increment";
            return;
        }

        if ((readResult() < 1))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Wrong increment";
            return;
        }

        // read size of array
        uint32_t size;
        if (!read((char *)&size, sizeof(uint32_t)))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << " Unable to read size of array";
            return;
        }

        size = ::ntohl(size);
        BOOST_LOG_TRIVIAL(info) <<_pid << " Size of array = " << size;

        // README: I see no point in dividing a huge array into blocks.
        // In any case, the system performs well with tens of gigabytes of dedicated memory.
        // But we must choose a strategy - optimize the program for speed or memory consumption.
        std::unique_ptr< double[] > double_buffer = std::make_unique< double[] >(size);
        size_t header = size * sizeof(double);
        if (!read((char *)&double_buffer[0], header))
        {
            BOOST_LOG_TRIVIAL(error) <<_pid << "Unable to read array";
            return;
        }

        std::string file = "./ozzylogik" + std::to_string(getpid());
        std::ofstream ostrm(file, std::ios::out | std::ios::trunc);
        for (size_t i = 0; i < size; ++i)
        {
            ntohd(double_buffer[i]);
            ostrm << double_buffer[i] << std::endl;
        }

        return;
    } // while (_connect)
}
