/*
Copyright (C) 2021 by Alex Perchatochnikov <aperon.all@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config.hpp"
#include "cltmain.hpp"
#include "cltclt.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <iostream>
#include <string>
#include <signal.h>
#include <stdint.h>

static constexpr uint16_t default_port = 8765;
static const std::string default_addr  = "127.0.0.1";

static Client *client = nullptr;

static void sig_handler (int signum)
{
    if (signum == SIGINT || signum == SIGTERM)
    {
        std::cout << "\nReceived signal Ctrl-C" << std::endl;
        BOOST_LOG_TRIVIAL(info) << getpid() << " Received signal Ctrl-C";

        if (client)
            client->stop();
    }
}

int main ([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
{
    uint16_t port = 0;
    std::string str_addr;

    std::cout << "ozzylogik client"
              << OZZYLOGIK_VERSION_STRING
              << "  Copyright (C) 2021 aperon.all@gmail.com" << std::endl;

    // init log
    logging::add_file_log ( keywords::file_name = "ozzylogik" + std::to_string(getpid()) + "_%N.log",
                            keywords::rotation_size = 10 * 1024 * 1024,     // rotate files every 10 MiB or at midnight
                            keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                            keywords::auto_flush = true,
                            keywords::format = "[%TimeStamp%]: %Message%"); // log record format

    logging::core::get()->set_filter( logging::trivial::severity >= logging::trivial::info);
    logging::add_common_attributes();

    // start logging
    BOOST_LOG_TRIVIAL(info) << getpid() << " Start client";

    // get config
    try
    {
        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini("ozzylogik.conf", pt);
        std::string str_port = pt.get< std::string >("Server.port");
        str_addr = pt.get< std::string >("Server.addr");

        if (!str_port.empty())
        {
            uint16_t value;

            try
            {
                value = boost::lexical_cast< uint16_t >(str_port);
                port = value;
            }
            catch (boost::bad_lexical_cast const &e)
            {   // bad input
                BOOST_LOG_TRIVIAL(error) << getpid() << " Bad config file";
            }
        }
        else
        {
            port = default_port;
        }

        if (!str_addr.empty())
            str_addr = default_addr;
    }
    catch (...)
    {
        port = default_port;
        str_addr = default_addr;
    }

    BOOST_LOG_TRIVIAL(info) << getpid() << " Config file is ready";

    // FIXME: if there is an exception at this place, then it's pointless to restore something
    // How can I pass this to sig_handler function?
    // std::unique_ptr< Client > client = std::make_unique< Client >(port);
    client = new Client( port, str_addr);
    if (!client->init())
    {
        BOOST_LOG_TRIVIAL(error) << getpid() << " Fatal error into client";
        return 1;
    }

    // Ctrl-C catch
    signal( SIGINT,  sig_handler);
    signal( SIGTERM, sig_handler);

    signal( SIGPIPE, sig_handler);

    client->start();
    delete client;

    BOOST_LOG_TRIVIAL(info) << getpid() << " Stop client";
}
